# stevepatter/laravel-gallery

Laravel gallery with S3 storage

Initial requirements

* albums
* each image will be in an album
* album directories map exactly to S3
* cached thumbnails (files stored in album subdirectories in S3) 
* image base URL setting, so you can serve images directly from S3 or via CloudFront
* admin views/functionality added (for managing albums, images etc). Routing for these to 
  use standard `auth` middleware, though actual login/logout is not handled by this package & should be handled externally

## Environment Variables

* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`
* `AWS_DEFAULT_REGION`
* `AWS_BUCKET`
* `AWS_URL`


## Configuration

The default base path is `/gallery`. To change this, publish configuration
and edit the file 'config/gallery.php'  

## Installation

composer require stevepatter/laravel-gallery

php artisan vendor:publish

*Importing CSS* add `@import "../../vendor/stevepatter/laravel-gallery.scss";` to laravel's app.scss & run `npm run prod`

# this is wrong, keep for now
*Importing Javascript* add `require('ekko-lightbox');` to laravel's bootstrap.js 

## TODO

* admin functions

* laravel queue for thumbnailing
* files uploaded to S3 should be set with a long cache expiration time
* queue should be paralellised
    * https://laracasts.com/discuss/channels/laravel/running-multiple-queue-workers
    * https://ohdear.app/blog/how-to-size-scale-your-laravel-queues

### Admin Functionality Required

* create (an empty) album (REST only)
* display a list of albums
* set the featured image for an album
* delete an album and images it contains
* upload single image to album
* upload multiple images to an existing album
* upload multiple images & create new album at the same time