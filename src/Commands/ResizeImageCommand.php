<?php

namespace StevePatter\LaravelGallery\Commands;

use Illuminate\Console\Command;
use StevePatter\LaravelGallery\Models\Photo;

class ResizeImageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:resize {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resize & cache image';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $photo = Photo::where('id', $this->argument('id'))->first();
        $photo->checkCachedImage();
    }
}
