<?php

namespace StevePatter\LaravelGallery\Commands;

use Illuminate\Console\Command;
use StevePatter\LaravelGallery\Models\Photo;

class ResizeAllImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:resizeall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resize all images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Photo::all() as $one) {
            $one->checkCachedImage();
        }
    }
}
