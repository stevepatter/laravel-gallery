<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 24/11/18
 * Time: 13:42
 */

return [

    /**
     * AWS Configuration
     */
    'AWS_ACCESS_KEY_ID' => env('AWS_ACCESS_KEY_ID'),
    'AWS_SECRET_ACCESS_KEY' => env('AWS_SECRET_ACCESS_KEY'),
    'AWS_S3_BUCKET' => env('AWS_S3_BUCKET'),

    /**
     * Base URL
     */
    'base_path' => 'gallery',

    /**
     * Layout view to use
     */
    'layout' => 'layouts.laravel-default',
    
    /**
     * Preview configuration
     */
    'preview' => [
        'width' => 600,
        'height' => 600
    ]
];