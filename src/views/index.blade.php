{{-- display the list of albums --}}
@extends('layouts.app')

@section('content')

    <div class="row masonry">
    @foreach ($models as $one)
        <div class="grid">
            <img src="{{ $one->cover->preview() }}"></a>
            <div class="grid__body">
                <div class="relative">
                    <a class="grid__link" target="_blank" href="{{
                        route('gallery::album', [
                            'id' => $one->id, 'name' => $one->name
                        ]) }}" ></a>
                    <h1 class="grid__title">{{ $one->name }}</span>
                    <p class="grid__author">Stephen Patterson</p>
                </div>
            <div class="mt-auto" >
              <span class="grid__tag">{{ $one->created_at->format('d/m/y') }}</span>
            </div>
        </div>
    @endforeach
  </div>
    
  @include('gallery::components.pagination', ['models'  => $models])

@endsection
