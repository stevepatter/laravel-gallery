{{--  Display the photos within an album --}}
@extends('layouts.app')

@section('content')
    <header class="masthead gallery" style="background-image: url('{{ $album->cover->preview() }}')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>{{ $album->name }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="row album-list">
        @foreach ($photos as $one)
        	<a href="{{ $one->url() }}" data-toggle="lightbox" class="col-xs-12 col-md-6 preview-image">
                <div style="background-image: url('{{ $one->preview() }}'); background-size: cover; height:350px;">
                    <div class="footer">
                        <span class="title">{{ $one->title }}</span>
                        <span class="date">{{ $one->created_at->format('d/m/y') }}</span>
                    </div>
                </div>
            </a>
        @endforeach
    </div>

    @include('gallery::components.pagination', ['models'  => $photos])

@endsection
