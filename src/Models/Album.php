<?php

namespace StevePatter\LaravelGallery\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model {

    public $fillable = [
        'name',
        'cover_photo_id'
    ];

    public function photos() {
        return $this->hasMany(Photo::class);
    }

    /**
     * Cover Image. Default to first in album if unset
     * @return Photo
     */
    public function cover() {
        if (!empty($this->cover_photo_id)) {
            return $this->belongsTo(Photo::class, 'cover_photo_id', 'id');
        } else {
            // fallback in case cover photo is not set
            return $this->hasOne(Photo::class)->where('album_id', $this->id);
        }
    }

    /**
     * Initial public list
     * @return mixed
     */
    public static function published() {
        return self::whereNull('deleted_at')->orderBy('created_at', SORT_DESC);
    }
}