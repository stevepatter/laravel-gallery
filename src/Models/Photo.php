<?php

namespace StevePatter\LaravelGallery\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Storage;

class Photo extends Model {
    public $fillable = [
        'album_id',
        'filename',
        'title',
        'description'
    ];

    public function album() {
        return $this->belongsTo(Album::class);
    }

    /**
     * URL to full size image
     * @return string
     */
    public function url() {
        return config('filesystems.disks.s3.url') . $this->filename;
    }
    
    /**
     * Get URL of image at desired size
     *
     * @param $width
     * @param $height
     * @return $url string
     */
    public function preview($width=null, $height=null) {
        if (! $width && ! $height) {
            $width = config('gallery.preview.width');
            $height = config('gallery.preview.width');
        }

        $resizefilename = $this->generateResizeFilename($width, $height);

        return config('filesystems.disks.s3.url') . $resizefilename;
    }

    /**
     * Generate base filename to resize as
     * @param integer $width
     * @param integer $height
     * @return string
     */
    protected function generateResizeFilename($width=null, $height=null) {
        return sprintf('sizes/%dx%d_%s',
            $width,
            $height,
            $this->filename
        );
    }

    /**
     * Generate cached preview/thumbnail image
     *
     * @param integer $width
     * @param integer $height
     */
    public function checkCachedImage($width=null, $height=null) {
        $resize = false;
        if (! $width && ! $height) {
            $width = config('gallery.preview.width');
            $height = config('gallery.preview.width');
        }
        $resizefilename = $this->generateResizeFilename($width, $height);

        // not cached
        if (! Storage::disk('s3')->exists($resizefilename)) {
            $resize = true;
        }

        // size generated but more recent
        if ($resize === false && Storage::disk('s3')->lastModified($resizefilename) < Storage::disk('s3')->lastModified($this->filename)) {
            $resize = true;
        }

        if ($resize) {
            echo "Resizing to $resizefilename\n";
            $temp = sys_get_temp_dir() . '/temp-thumbnail';
            $s3Source = Storage::disk('s3')->get($this->filename);
            file_put_contents($temp, $s3Source);

            if ($res = $this->resizeImage($temp, $width, $height)) {
                if (sizeof($res) > 0) {
                    list($resizedImageResource, $mimeType) = $res;
                    switch ($mimeType) {
                        case "image/jpeg":
                            imagejpeg($resizedImageResource, $temp, 100);
                            break;
                        case "image/png":
                            imagepng($resizedImageResource, $temp, 100);
                            break;
                        case "image/gif":
                            imagegif($resizedImageResource, $temp, 100);
                            break;
                    }
                }
            }

            Storage::disk('s3')->put($resizefilename, file_get_contents($temp));
            if (file_exists($temp)) {
                unlink($temp);
            }
        } else {
            echo "resize already exists\n";
        }
    }

    /**
     * Actually perform resizing
     *
     * @param string $file filaneme
     * @param integer $w width
     * @param integer$h height
     * @param bool $crop
     * @return resource
     */
    protected function resizeImage($file, $w=null, $h=null, $crop = FALSE) {
        list($width, $height) = getimagesize($file);

        try {

            $r = $width / $height;
            if ($crop) {
                if ($width > $height) {
                    $width = ceil($width - ($width * abs($r - $w / $h)));
                } else {
                    $height = ceil($height - ($height * abs($r - $w / $h)));
                }
                $newwidth = $w;
                $newheight = $h;
            } else {
                if ($w / $h > $r) {
                    $newwidth = $h * $r;
                    $newheight = $h;
                } else {
                    $newheight = $w / $r;
                    $newwidth = $w;
                }
            }

            $mimeType = mime_content_type($file);
            switch ($mimeType) {
                case "image/jpeg":
                    $src = imagecreatefromjpeg($file);
                    break;
                case "image/png":
                    $src = imagecreatefrompng($file);
                    break;
                case "image/gif":
                    $src = imagecreatefromgif($file);
                    break;
                default:
                    // unrecognised image type, or not an image
                    return [];
            }

            $dst = imagecreatetruecolor($newwidth, $newheight);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

            $exif = exif_read_data($file);

            if (!empty($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    case 3:
                        $angle = 180;
                        break;

                    case 6:
                        $angle = -90;
                        break;

                    case 8:
                        $angle = 90;
                        break;
                    default:
                        $angle = 0;
                        break;
                }
                $bg = imagecolorallocatealpha($dst, 255, 255, 255, 127);
                $dst = imagerotate($dst, $angle, $bg);
            }


            return [$dst, $mimeType];
        } catch (\ErrorException $e) {
            echo "failed to create preview for " . $file;
            echo $e->getMessage();
        } catch (\Exception $e) {
            echo "failed to create preview for " . $file;
            echo $e->getMessage();
        }
    }
}
