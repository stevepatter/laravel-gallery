<?php

/**
 * Gallery Routing
 */
Route::prefix(config('gallery.base_path'))->namespace('StevePatter\LaravelGallery\Http\Controllers')->group(function() {

    /**
     * Public Routes
     */
    Route::get('/', 'GalleryController@index')->name('gallery::list');  // list of albums
    Route::get('/album/{name}/{id}', 'GalleryController@album')->name('gallery::album');    // view a single gallery
    
    /**
     * Private routes, for the admin interface (/gallery/admin)
     */
    Route::middleware('auth')->prefix('admin')->group(function() {
        
        // rest API endpoints
        Route::prefix('api')->group(function() {
            Route::resource('album', 'Admin\Api\AlbumController');
        });
        
        // non-rest endpoints & views
    });
    
});


