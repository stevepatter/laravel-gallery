<?php

namespace StevePatter\LaravelGallery\Http\Controllers;

use Illuminate\Routing\Controller;
use StevePatter\LaravelGallery\Models\Album;
use StevePatter\LaravelGallery\Models\Photo;

class GalleryController extends Controller {

    /**
     * List of all albums, paginated
     */
    public function index() {
        return view('gallery::index')->with('models', Album::published()->paginate(12));
    }

    /**
     * Display single album
     * @param $id
     * @param $name
     * @return mixed
     */
    public function album($name, $id) {
        $album = Album::findOrFail($id);
        return view('gallery::album', [
            'album'=> $album,
            'photos' => Photo::where('album_id', $album->id)->paginate(12)
        ]);
    }

}