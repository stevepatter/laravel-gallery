<?php
namespace StevePatter\LaravelGallery;

use StevePatter\LaravelGallery\Commands\ResizeImageCommand;
use StevePatter\LaravelGallery\Commands\ResizeAllImagesCommand;
use StevePatter\LaravelGallery\Commands\ResizeAlbumCommand;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider {

    public function register() {

    }

    public function boot() {
        // config file
        $this->publishes([
            __DIR__ . '/config/gallery.php' => config_path('gallery.php'),
        ], 'config');

        // css & js
        $this->publishes([
            __DIR__.'/assets' => resource_path('vendor/stevepatter'),
        ], 'assets');

        // Migrations
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        // package routes
        require __DIR__ . '/Http/routes.php';

        // commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                ResizeImageCommand::class,
                ResizeAllImagesCommand::class,
                ResizeAlbumCommand::class
            ]);
        }

        // views
        $this->loadViewsFrom(__DIR__.'/views', 'gallery');
    }
}